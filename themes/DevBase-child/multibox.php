 <!--Multibox Child-->
	<?php 
	$layout_title = get_sub_field( 'layout_title' );
	$layout_title = strtolower( str_replace( " ","", $layout_title ) );
	?>
	<div data-section-name="<?php echo esc_html( $layout_title ); ?>" class="content_row scrollify_pane waypoint <?php the_sub_field('multibox_class'); ?>">
 <!-- Desktop -->
		<div class="content_grid desktop" style="<?php the_sub_field('content_grid'); ?>">
			<?php if( have_rows('multibox_repeater') ):
			while ( have_rows('multibox_repeater') ) : the_row(); ?>
					<div class="content_box <?php the_sub_field('multibox_content_class'); ?>" style="<?php the_sub_field('multibox_style'); ?>" <?php if(get_sub_field('multibox_image_conditional') == "Repeated Image (tiled)"){ ?> background-image:url('<?php echo $image['url']; ?>');"<?php } ?>><!-- End of opening div tag -->
						<?php include( get_stylesheet_directory() . '/multibox_content.php'); ?>
						<div class="nav_down">
							<a href="#" class="scroll_next pinkpulse"><span class="fa fa-angle-down"></span></a>
						</div>
					</div>
			<?php endwhile;
			else :
			endif; ?>

		</div>
		<!-- Mobile -->
		<div class="content_grid mobile" style="<?php the_sub_field('content_grid_mobile'); ?>">
			<?php if( have_rows('multibox_repeater') ):
			while ( have_rows('multibox_repeater') ) : the_row(); ?>
				
				<?php if( get_sub_field('multibox_mobile_pane')){ ?>
					<div class="content_box <?php the_sub_field('multibox_content_class'); ?>" style="<?php the_sub_field('multibox_style_mobile'); ?>" <?php if(get_sub_field('multibox_image_conditional') == "Repeated Image (tiled)"){ ?> background-image:url('<?php echo $image['url']; ?>');"<?php } ?>><!-- End of opening div tag -->
						<?php include( get_stylesheet_directory() . '/multibox_content.php'); ?>
					</div>
				<?php } ?>
			<?php endwhile;
			else :
			endif; ?>
		</div>
	</div>
	<div class="content_row scrollify_pane scrollify_pane_placeholder <?php the_sub_field('multibox_class'); ?>">
		<div class="content_grid mobile" style="<?php the_sub_field('content_grid_mobile'); ?>">
			<?php if( have_rows('multibox_repeater') ):
			while ( have_rows('multibox_repeater') ) : the_row(); ?>
					<?php if(get_sub_field('multibox_mobile_pane_placeholder')){ ?>
						<div class="multibox_placeholder_content">
							<?php include( get_stylesheet_directory() . '/multibox_content_placeholder.php'); ?>
						</div>
					<?php } ?>
			<?php endwhile;
			else :
			endif; ?>
		</div>
	</div>

