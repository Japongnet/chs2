<?php
/*
Template Name: Custom Flexible Page
*/
?>

<?php get_header(); ?>

<?php include( get_stylesheet_directory() . '/menu-bar.php'); ?>

<div id="content_pane">
	<?php include( get_stylesheet_directory() . '/custom_flex_content.php'); ?>
</div>
<!-- / content_pane -->
<?php get_footer('new'); ?>
