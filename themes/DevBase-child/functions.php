<?php
/**  Queue parent style followed by child/customized style
 *
 * @package	 Devbase-Child
 * @author	  Julian Apong
 **/
function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
	}
	add_filter('upload_mimes', 'add_file_types_to_uploads');


function my_acf_admin_head() {
    ?>
	<style type="text/css">

	div[data-name=page_stack_image_repeater]{
		border:3px solid green !important;
	}

	div[data-name=multibox_repeater] .acf-row {
		border:3px solid green !important;
	}
	
	.acf-field.acf-field-repeater.acf-field-5be05e91ca86b{
		background: linear-gradient(to bottom, lightgreen, darkgreen);	
	}

	.acf-field.acf-field-repeater.acf-field-5be05e91ca86b .acf-input .acf-table .ui-sortable .acf-row{
		border-bottom: 3px solid green;
	}

	.acf-field.acf-field-flexible-content.acf-field-5953b7476514e{
	background: linear-gradient(to bottom, lightblue, #080838);
	}
	.layout > div:not([data-attribute="multibox"]){
	background: white;
	}

	button.collapse.collapse--flexi{
		top:0px;
	}

    </style>
    <?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');

function my_layout_title($title, $field, $layout, $i) {
	if($value = get_sub_field('layout_title')) {
		return $value;
	} else {
		foreach($layout['sub_fields'] as $sub) {
			if($sub['name'] == 'layout_title') {
				$key = $sub['key'];
				if(array_key_exists($i, $field['value']) && $value = $field['value'][$i][$key])
					return $value;
			}
		}
	}
	return $title;
}
add_filter('acf/fields/flexible_content/layout_title', 'my_layout_title', 10, 4);

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles', PHP_INT_MAX );

/** Let's enqueue the theme styles */
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/css/output.css', false, '0.1' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/output.css', array( 'parent-style' ), '0.1' );
}

add_filter( 'the_excerpt', 'shortcode_unautop' );
add_filter( 'the_excerpt', 'do_shortcode' );


/** Adding async handles to scripts
 *
 * @param tag	$tag Required. This makes it work.
 * @param handle $handle Required. This makes it work.
 */
function add_async_attribute_child( $tag, $handle ) {
	$scripts_to_async = array( 'production_child', 'jquery' );
	foreach ( $scripts_to_async as $async_script ) {
		if ( $async_script === $handle ) {
			return str_replace( ' src', ' defer="defer" src', $tag );
		}
	}
	return $tag;
}
/** Ajax Post loading */
function dynamic_load_posts() {
	// WP_Query arguments.
	if ( isset( $_REQUEST['menu_id'] ) ) {
		$args = array(
			'page_id' => sanitize_text_field( wp_unslash( $_REQUEST['menu_id'] ) ),
		);
	}
	// The Query.
	$query = new WP_Query( $args );

	// The Loop.
	ob_start();
	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			include get_stylesheet_directory() . '/custom_flex_content.php';
		}
	}
	$single_page = ob_get_contents();
	ob_end_clean();
	// Restore original Post Data.
	wp_reset_postdata();
	echo $single_page;
	die();
}

add_filter('nav_menu_link_attributes', 'menu_post_ids');
function menu_post_ids($val){
	$postid = url_to_postid( $val['href'] );
	$val['data-postid'] = $postid;
	return $val;
}

add_filter( 'script_loader_tag', 'add_async_attribute_child', 10, 2 );


add_action( 'wp_enqueue_scripts', 'myplugin_enqueue' );

function myplugin_enqueue() {
 // Need to fix this for W3C compliance...
}

wp_register_script( 'jquery', get_stylesheet_directory_uri() . '/js/lib/jquery.min.js', false, null );
wp_enqueue_script( 'jquery' );

wp_register_script( 'production_child',  get_stylesheet_directory_uri() . '/js/build/production_child.min.js', array(), '' ); // Our concat production files.
wp_enqueue_script( 'production_child' ); // Enqueue it!

add_filter('style_loader_tag', 'myplugin_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'myplugin_remove_type_attr', 10, 2);

function myplugin_remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}

/*
wp_localize_script( 'production_child', 'dynamic_load_posts', array(
	'ajaxurl' => admin_url( 'admin-ajax.php' )
));
*/

add_action( 'wp_ajax_nopriv_dynamic_load_posts', 'dynamic_load_posts' );
add_action( 'wp_ajax_dynamic_load_posts', 'dynamic_load_posts' );
