<?php
/**  Ajax append next page to end of a page sequence
 *
 * @package Devbase-Child
 * @author Julian Apong
 **/

?>
<!-- next_page.php-->
<?php if ( get_field( 'next_post' ) ) : ?>
	<div class="content_row scrollify_pane waypoint" data-section-name="last">
		<h2>NEXT</h2>
			<?php $post_url = get_field( 'next_post' ); ?>
			<?php $postid = url_to_postid( $post_url ); ?>
			<?php echo 'Post URL:' . $post_url . '<br/>'; ?>
			<?php echo 'Post ID:' . $postid . '<br/>'; ?>
	</div>
<?php endif; ?>
<div class="content_row scrollify_pane waypoint">
	<a href="<?php echo $post_url; ?>" class="waypoint_next" data-href="<?php echo $postid; ?>">Waypoint</a>
</div>
<!-- /next_page.php-->
