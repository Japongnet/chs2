<?php get_header();

include 'header_menu.php';

 ?>


<div class="section bg_cover <?php the_field('blog_section_class'); ?>" style="background-image:url('<?php the_field('blog_section_background');?>')">
  <div class="section_content">
    <div class="section_content <?php the_field('blog_section_orientation'); ?>">
      <div class="section_content_main ">
        <div class="section_content_text">
          <h2  class="section_title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
          </h2><br/>

          <?php if( get_field('blog_section_tagline') ): ?>
            <em class="section_tagline"><?php the_field('blog_section_tagline'); ?></em>
          <?php endif; ?>
          <br/><span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
          <hr/>
          <?php if( get_field('blog_section_text') ): ?>
            <div class="section_text"><?php the_field('blog_section_text'); ?></div>
          <?php endif; ?>
          <?php if( get_field('blog_section_button') ): ?>
            <button class="section_button"><?php the_field('blog_section_button'); ?></button>
          <?php endif; ?>
        </div>

      </div>


    </div>

  </div>
</div>


<div id="content_area">
  <!-- section -->





</div>








<?php get_footer(); ?>
