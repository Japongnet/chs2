<?php
/**  Ajax append prev page to end of a page sequence
 *
 * @package Devbase-Child
 * @author Julian Apong
 **/

?>
<!-- prev_page.php-->
<?php if ( get_field( 'prev_post' ) ) : ?>
	<?php $post_url = get_field( 'prev_post' ); ?>
	<?php $postid = url_to_postid( $post_url ); ?>
<?php endif; ?>


<h2>PREV</h2>
<div class="content_row scrollify_pane waypoint" data-section-name="first">
	<a href="<?php echo $post_url; ?>" class="waypoint_prev" data-href="<?php echo $postid; ?>">Waypoint</a>
</div>
<?php if ( get_field( 'prev_post' ) ) : ?>
	<div class="content_row scrollify_pane waypoint">
			<?php $post_url = get_field( 'prev_post' ); ?>
			<?php $postid = url_to_postid( $post_url ); ?>
			<?php echo 'Post URL:' . $post_url . '<br/>'; ?>
			<?php echo 'Post ID:' . $postid . '<br/>'; ?>
	</div>
<?php endif; ?>

<!-- /prev_page.php-->