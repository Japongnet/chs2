<?php
/** Main Nav menu bar
 * 
 * @package Devbase-Child
 * @author Julian Apong
 **/

?>

<div class="menu_bar">
	<div id="menu_pane">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="nav_toggle">
				<div class="nav_social">
				<a href="#cascading" class="home_link desktop">CASCADING DESIGN</a>
					<a href="https://twitter.com/CascadingDesign" aria-label="Cascading Design on Twitter"><span aria-hidden="true" class="fa fa-twitter"></span></a>
					<a href="mailto:japong@gmail.com" aria-label="Email Cascading Design"><span aria-hidden="true" class="fa fa-envelope"></span></a>
				</div>
				<!--
				<a class="navbar-brand mobile" href="#">Cascading</a>
				<button class="navbar-toggler mobile" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					<span class="fa fa-bars"></span>				</button>
				-->
			</div>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
			
			<?php /*
				
				// WP_Query arguments
				$args = array(
					'post_type'              => array( 'page' ),
				);

				// The Query
				$query = new WP_Query( $args );

				// The Loop
				
				if ( $query->have_posts() ) { ?>
					<ul  class="navbar-nav mr-auto">
						<li class="nav-item"><a class="nav-link" href="<?php echo get_site_url(); ?>">CASCADING DESIGN</a></li>
						<?php 
							while ( $query->have_posts() ) {
								$query->the_post(); ?>
						
							<li class="nav-item"><a class="nav-link" href="<?php the_permalink(); ?>" data-href="<?php the_ID(); ?>"><?php the_title(); ?></a></li>
							
						<?php }
						} else {
						// no posts found
						} ?>
						
					</ul>
				<?php

				// Restore original Post Data
				wp_reset_postdata();
				
				*/ ?>
			</div>
		</nav>

		
		
	</div>
</div>