

<?php /* include( get_stylesheet_directory() . '/prev_page.php' ); */ ?>
<?php include( get_stylesheet_directory() . '/background_stack.php'); ?>
	
	<?php

	$scrollcounter = 0;
	// check if the flexible content field has rows of data
	if( have_rows('flex_content') ):
	// loop through the rows of data
	while ( have_rows('flex_content') ) : the_row();
	?> 

			<?php if( get_row_layout() == 'multibox' ): ?>
				<?php include( get_stylesheet_directory() . '/multibox.php'); ?>
				<?php $scrollcounter++ ?>
			<?php endif; 

	endwhile;
		
	else :

		// no layouts found

	endif;

	?>
						<!-- next: custom_flex_content -->
		<?php /* include( get_stylesheet_directory() . '/next_page.php'); */ ?>
