<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>


		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
		
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>


		<style>

  @media not all and (min-resolution:.001dpcm) { @supports (-webkit-appearance:none) { body { background: rgb(76,80,84) !important; } #bg_backdrop { background: transparent !important; } } }

</style>
	</head>
	<body <?php body_class(); ?>>
	
	
	<svg width="0" height="0" style="position:absolute">
  <defs>
    <clipPath id="svg_rectangles" clipPathUnits="objectBoundingBox">
<polygon points="0 0, 0 0.2997, 1 0.2997, 1 0"></polygon>
<polygon points="0 .3561, 1 0.3561, 1 0.6418, 0 0.6418"></polygon>
<polygon points="0 .7013, 1 0.7013, 1 1, 0 1"></polygon>
</clipPath>
  </defs>
</svg>


		<!-- header -->



		<!-- /header -->
