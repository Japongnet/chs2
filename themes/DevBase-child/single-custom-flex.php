<?php
/** Custom Flex Post
 *
 * Template Name: Custom Flexible Post
 * Template Post Type: post, page, product
 *
 * @package Devbase-Child
 * @author Julian Apong
 */

?> 

<?php get_header(); ?>


<div class="menu_bar">
	<div id="menu_pane">
		<?php
		// WP_Query arguments
		$args = array(
			'post_type'              => array( 'post' ),
		);

		// The Query
		$query = new WP_Query( $args );

		// The Loop
		if ( $query->have_posts() ) { ?>
			<ul>
			<?php
				while ( $query->have_posts() ) {
					$query->the_post(); ?>
			
				<li><a href="<?php the_permalink(); ?>" data-href="<?php the_ID(); ?>"><?php the_title(); ?></a></li>
				
			<?php }
			} else {
			// no posts found
			} ?>
			</ul>
		<?php

		// Restore original Post Data
		wp_reset_postdata();
		?>
	</div>
</div>
<div id="content_pane">
	<?php include( get_stylesheet_directory() . '/background_stack.php' ); ?>
	<?php include( get_stylesheet_directory() . '/prev_page.php' ); ?>
	<?php


	// check if the flexible content field has rows of data
	if( have_rows('flex_content') ):
	
	// loop through the rows of data
	while ( have_rows('flex_content') ) : the_row(); 
	
	?>

	


	<?php if( get_row_layout() == 'full_width' ):  ?>




	<div class="content_row waypoint <?php the_sub_field('full_width_content_class'); ?>
		<?php if(get_sub_field('black_overlay')): ?>
			<div class="black_overlay"></div>
		<?php endif;
		?>
		<?php the_sub_field('full_width_image_class'); ?>"
		<?php if(get_sub_field('full_width_image_conditional') == "Repeated Image (tiled)"){
			$image = get_sub_field('full_width_image');
			?>
			style="background-image:url('<?php echo $image['url']; ?>');"
			<?php } ?> >
			<?php if(get_sub_field('full_width_image_conditional') != "No Image"){
				$image = get_sub_field('full_width_image');
			} ?>
			<div class="content_wrapper">
				<?php the_sub_field('full_width_content'); ?>
			</div>
			<?php if(get_sub_field('full_width_image_conditional') == "Background Photo"){ ?>
				<?php if(get_sub_field('full_width_image')){
					$image = get_sub_field('full_width_image'); ?>

					<img class="background_image content_wrapper_image <?php the_sub_field('full_width_image_class'); ?>" src="<?php echo $image['sizes']['thumbnail']; ?>"  srcset="<?php echo $image['sizes']['medium'] .' '. $image['sizes']['medium-width'] .'w,'; ?>  <?php echo $image['sizes']['large'] .' '. $image['sizes']['large-width'] .'w, '; ?> <?php echo $image['url'] .' '. $image['width'] .'w '; ?>" sizes="100vw" alt="<?php echo $image['title'] ?>" />
					<?php } ?>
					<?php  } ?>
				</div>

			<?php elseif( get_row_layout() == 'multibox' ): ?>

				<?php include( get_stylesheet_directory() . '/multibox.php'); ?>




			<?php elseif( get_row_layout() == 'link_repeater' ): ?>

				<?php include 'link_repeat.php'; ?>

			<?php elseif( get_row_layout() == 'post_embed' ): ?>

				<?php include 'post_embed.php'; ?>

			<?php elseif( get_row_layout() == 'post_slider' ): ?>

				<?php include 'post_slider.php'; ?>


			<?php endif;


				
		endwhile;

		else :

			// no layouts found

		endif;

		?>
				<!-- next: post-custom-flex-->
		<?php include( get_stylesheet_directory() . '/next_page.php'); ?>

		
</div>
		<?php get_footer('new'); ?>
