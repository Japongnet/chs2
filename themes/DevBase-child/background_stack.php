<!-- Add up sizes for aspect ratio -->
<?php if( have_rows('page_stack') ):
	while ( have_rows('page_stack') ) : the_row();
			while ( have_rows('page_stack_image_repeater') ) : the_row(); 
					$image_id = get_sub_field('page_stack_image');
					$image_sizes = array('small_custom', 'medium_custom', 'large_custom', 'full');
					$srcset = "";
					$srcset_webp = "";
					$width_size_stack[] = wp_get_attachment_image_src($image_id, $image_size)[1];
					$height_size_stack[] = wp_get_attachment_image_src($image_id, $image_size)[2];

			endwhile;
	$height_size_stack_max = max($height_size_stack);
	$width_size_stack_max = max($width_size_stack);
	// echo '$height_size_stack_max: ' . $height_size_stack_max . '!<br/>';
	unset($height_size_stack);
	$height_size_total += $height_size_stack_max;
	endwhile;
endif;
// echo 'height_size total:' . $height_size_total . '<br/>';
// echo 'width_size max:' . $width_size_stack_max . '<br/>';
unset($width_size_stack);
unset($height_size_stack);
?>

<div id ="bg_backdrop">
</div>

<div  id="bg_pane" class="bg_fixed" style="--stack_width: <?php echo $width_size_stack_max; ?>; --stack_height: <?php echo $height_size_total ?>">
	<div class="aspectratio" style="--aspect-ratio: <?php echo $width_size_stack_max . '/' . $height_size_total ?> ;">
			<div class="bg_stack" style="<?php the_field('page_stack_grid'); ?>" >
					<?php if( have_rows('page_stack') ):
							$stack_timing_factor = 0.1;
							$i = 1;

							while ( have_rows('page_stack') ) : the_row(); 
							?>
							<!-- Get max sizes for grid_items -->
							<?php
									if( have_rows('page_stack_image_repeater') ):
											while ( have_rows('page_stack_image_repeater') ) : the_row();
													$image_id = get_sub_field('page_stack_image');
													$grid_width_size_stack[] = wp_get_attachment_image_src($image_id, $image_size)[1];
													$grid_height_size_stack[] = wp_get_attachment_image_src($image_id, $image_size)[2];
												endwhile;
												$grid_width_size_stack_max = max($grid_width_size_stack);
												$grid_height_size_stack_max = max($grid_height_size_stack);
												unset($grid_width_size_stack);
												unset($grid_height_size_stack);
									endif; ?>

							<!-- Display the stack -->
							<div id="page_stack_id_<?php echo $i ?>" class="page_stack_grid_item <?php the_sub_field('page_stack_item_class'); ?>" style="--grid_item_width: <?php echo $grid_width_size_stack_max; ?>; --grid_item_height: <?php echo $grid_height_size_stack_max; ?>; <?php echo '--stack_timing_factor:' . $stack_timing_factor . '; ';  the_sub_field('page_stack_item_style'); ?>">
									<?php if( have_rows('page_stack_image_repeater') ):
									$element_timing_factor = 0;
									while ( have_rows('page_stack_image_repeater') ) : the_row();
									?>
									
											<?php 
											$image_id = get_sub_field('page_stack_image');
											$width_size_stack[] = wp_get_attachment_image_src($image_id, $image_size)[1];
											$height_size_stack[] = wp_get_attachment_image_src($image_id, $image_size)[2];
											$image_sizes = array('small_custom', 'medium_custom', 'large_custom', 'full');
											$srcset = "";
											$srcset_webp = "";
											$upload_dir = wp_upload_dir();
											$upload_dir['baseurl'] = str_replace('http', 'https', $upload_dir['baseurl']);
											unset( $srcset_array );
											foreach ( $image_sizes as &$image_size ) {
												$srcset_array[] = wp_get_attachment_image_src( $image_id, $image_size );
												/* echo '<br/> $image_size: ' . $image_size;
												echo '<br/> $srcset_array[0]: ' . $srcset_array[0];
												echo '<br/> $srcset_array[1]: ' . $srcset_array[1]; */
											}
											$temp = array_unique( array_column( $srcset_array, 0 ) );
											$srcset_array_unique = array_intersect_key( $srcset_array, $temp );
											foreach ( $srcset_array_unique as &$srcset_array_entries ) {
												$srcset .= $srcset_array_entries[0] . ' ' . $srcset_array_entries[1] . 'w, ';
											}
											$srcset = substr( $srcset, 0, -2 );
											$image_info = wp_get_attachment_image_src($image_id, 'medium_custom');
											$image_filetype = wp_check_filetype( $image_info[0]);
											if ( $image_filetype['ext'] !== 'svg') {
												foreach ( $srcset_array_unique as &$srcset_array_entries ) {
													$srcset_webp .= $srcset_array_entries[0] . '.webp ' . $srcset_array_entries[1] . 'w, ';
												}
											}
											$srcset_webp = substr( $srcset_webp, 0, -2 );
											/*
											foreach ( $image_sizes as &$image_size ) {
													if( !next( $image_sizes ) ) {
															$image_info = wp_get_attachment_image_src($image_id, 'medium_custom');
															$image_filetype = wp_check_filetype( $image_info[0]);
																$srcset .= wp_get_attachment_image_src($image_id, $image_size)[0] .' ' . wp_get_attachment_image_src($image_id, $image_size)[1] .'w ';

															if ( $image_filetype['ext'] !== 'svg') {
																	$srcset_webp .= wp_get_attachment_image_src($image_id, $image_size)[0] .'.webp ' . wp_get_attachment_image_src($image_id, $image_size)[1] .'w ';
															}
													} else {
															$srcset .= wp_get_attachment_image_src( $image_id, $image_size )[0] .' ' . wp_get_attachment_image_src( $image_id, $image_size )[1] . 'w, ';
															$image_info = wp_get_attachment_image_src($image_id, 'medium_custom');
															$image_filetype = wp_check_filetype( $image_info[0]);
																$srcset .= wp_get_attachment_image_src($image_id, $image_size)[0] .' ' . wp_get_attachment_image_src($image_id, $image_size)[1] .'w ';
																if ( $image_filetype['ext'] !== 'svg') {
																	$srcset_webp .= wp_get_attachment_image_src($image_id, $image_size)[0] .'.webp ' . wp_get_attachment_image_src($image_id, $image_size)[1] .'w, ';
																}
													}
											} 
											*/
											?>
											<div class="page_stack_element 
													<?php if ( get_sub_field( 'page_stack_element_class' ) ) :
															the_sub_field( 'page_stack_element_class' );
													endif; ?>"
											style="<?php the_sub_field( 'page_stack_image_style' ); ?> <?php echo '--element_timing_factor:' . $element_timing_factor . '; '?>" >
													<?php if ( get_sub_field( 'page_stack_image' ) ) { ?>
													<?php $image_info = wp_get_attachment_image_src($image_id, 'medium_custom'); ?>
													<?php $image_filetype = wp_check_filetype( $image_info[0]); ?>
													<?php /* echo '<pre>'; ?>
															<?php print_r($image_info); ?>
															<?php echo $image_filetype['ext'] ?>
													<?php echo "</pre>"; */ ?>

															<picture class="lazy">
															<?php if ( '' != $srcset_webp ) { ?>
																	<source srcset="<?php echo $srcset_webp ?>" sizes="(max-width: 320px) 280px, (max-width: 480px) 440px, 800px" type="image/webp"/>
															<?php } ?>
															<?php  if ( $image_filetype['ext'] == 'jpeg' ) { ?>
																	<source srcset="<?php echo $srcset ?>" sizes="(max-width: 320px) 280px, (max-width: 480px) 440px, 800px" type="image/jpeg"/>
															<?php } elseif ($image_filetype['ext'] == 'png') { ?>
																	<source srcset="<?php echo $srcset ?>" sizes="(max-width: 320px) 280px, (max-width: 480px) 440px, 800px" type="image/png"/>
															<?php } elseif ($image_filetype['ext'] == 'svg') { ?>

																	<?php } else { ?>
																	<?php } ?>

																	<img class="<?php the_sub_field( 'page_stack_image_class' ); ?>" alt="" src="<?php echo wp_get_attachment_image_src($image_id, 'large_custom')[0] ?>"/>
															</picture>
													<?php } ?>
													<?php if ( get_sub_field( 'page_stack_content' ) ) : ?>
															<div class="<?php the_sub_field( 'page_stack_image_class' ); ?>">
																	<?php the_sub_field( 'page_stack_content_title' ); ?>
																	<div class="page_stack_content">
																			<?php the_sub_field( 'page_stack_content' ); ?>
																	</div>
															</div>
													<?php endif; ?>
											</div>
											<?php 
											$element_timing_factor = $element_timing_factor + 0.1;
											endwhile;

									else :
									endif; ?>
									
							</div>
							
							<?php 
							$stack_timing_factor = $stack_timing_factor + 0.1;
							$i = $i + 1;
							endwhile;
							else :
							endif;
					?>
			</div>
	</div>
</div>