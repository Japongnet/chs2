(function ($, root, undefined) {
	$(function () {
		'use strict';
		// DOM ready, take it away
	});
})(jQuery, this);

function waypoint_directions(){
	var waypoints = jQuery('.waypoint').waypoint(function(direction) {
		if (direction === 'down') {
			jQuery('.waypoint_active').removeClass('waypoint_active');
			jQuery(this.element).addClass('waypoint_active');
			
		}
	}, {
		offset: '50%'
	});

	var waypoints = jQuery('.waypoint').waypoint(function(direction) {
		if (direction === 'up') {
			jQuery('.waypoint_active').removeClass('waypoint_active');
			jQuery(this.element).addClass('waypoint_active');
		}
		}, {
		offset: '-25%'
		}
	);
};


function waypoint_next(){
	
	var waypoints_next = jQuery('.waypoint_next').waypoint(function(direction) {
		if (direction === 'down') {
			// console.log('waypoint_next down!');
			waypoint_href = '';
			waypoint_url = '';
			var waypoint_href= $('.waypoint_active .waypoint_next').attr('data-href');
			var waypoint_url= $('.waypoint_active .waypoint_next').attr('href');
			// console.log( waypoint_href );
			// console.log( waypoint_url );
			$.ajax({
				url: dynamic_load_posts.ajaxurl,
				type: 'post',
				data: {
					action: 'dynamic_load_posts',
					'menu_id': waypoint_href
				},
				success: function( result ) {
					page_transition_load_init( result, waypoint_url );
					jQuery.scrollify.instantMove("#2");
				},
				error: function( result ) {
					alert( 'error' );
				}
			})
		}
	}, {
		offset: '50%'
		}
	);
};

function page_transition_load_init( result, waypoint_url ){
	Waypoint.destroyAll();
	jQuery.scrollify.destroy();
	// console.log( 'Ajax Triggered!' );
	jQuery('#content_pane').empty();
	// console.log( 'empty!' );
	jQuery('#content_pane').append(result);
	waypoint_directions();
	swap_setup();
	scrollify_setup();
	checkSize();
	jQuery('.waypoint_active').removeClass('waypoint_active');
	window.history.pushState("next_page", "Title", waypoint_url);
	swap_image();
}

function waypoint_prev(){
	
	var waypoints_prev = jQuery('.waypoint_prev').waypoint(function(direction) {
		if (direction === 'up') {
			console.log('waypoint_prev up!');
			waypoint_href = '';
			waypoint_url = '';
			var waypoint_href= $('.waypoint_active .waypoint_prev').attr('data-href');
			var waypoint_url= $('.waypoint_active .waypoint_prev').attr('href');
			// console.log( 'Waypoint Href:' + waypoint_href + '!' );
			// console.log( 'Waypoint url:' + waypoint_url + '!' );
			$.ajax({
				url: dynamic_load_posts.ajaxurl,
				type: 'post',
				data: {
					action: 'dynamic_load_posts',
					'menu_id': waypoint_href
				},
				success: function( result ) {
					page_transition_load_init( result, waypoint_url );
					jQuery.scrollify.instantMove("#last");
				},
				error: function( result ) {
					alert( 'error' );
				}
			})
		}
	}, {
		offset: '0%'
		}
	);
};

function swap_image(){
	jQuery(document).on("swap_image", function () {
		// console.log('Swap Image Started!');
		jQuery('#bg_pane source.placeholder_image').each(function () {
			jQuery('#bg_pane source.placeholder_image').fadeOut();
			var $this = jQuery(this),
			newSrc = $this.attr('data-srcset');
			$this.attr('srcset', newSrc);
			// console.log('newSrc: ' + newSrc);
			
			jQuery('#bg_pane source.placeholder_image').fadeIn();
			jQuery( '#bg_pane source.placeholder_image' ).removeClass( "placeholder_image" );
			jQuery( '#bg_pane source.placeholder_image' ).removeClass( "placeholder_image" );		
		});
		jQuery('.waypoint_loaded source.placeholder_image').each(function () {
			jQuery('.waypoint_loaded picture.placeholder_image').fadeOut();
			var $this = jQuery(this),
			newSrc = $this.attr('data-srcset');
			$this.attr('srcset', newSrc);
			// console.log('newSrc: ' + newSrc);
			jQuery('.waypoint_loaded picture.placeholder_image').fadeIn();
			jQuery( ".waypoint_loaded picture.placeholder_image" ).removeClass( "placeholder_image" );
			jQuery( ".waypoint_loaded source.placeholder_image" ).removeClass( "placeholder_image" );		
		});
		jQuery('.waypoint_loaded.placeholder_bg').each(function () {
			var $this = jQuery(this),
			newSrc = $this.attr('data-alternate-src');
			$this.fadeOut(  function() {
				$this.attr('style', newSrc);
			});
			$this.fadeIn();
			jQuery( ".waypoint_loaded.placeholder_bg").removeClass( "placeholder_bg" );
		});
	});
};

function checkSize(){
	
	if (jQuery(".content_row").css("flex-direction") == "row" ){
		jQuery('.scrollify_pane_placeholder').removeClass('scrollify_pane');
		jQuery.scrollify.update();
	}
	else {
		jQuery('.scrollify_pane_placeholder').addClass('scrollify_pane');
		jQuery.scrollify.update();
	}
};

function scrollify_setup(){
	jQuery.scrollify({
		section : ".scrollify_pane",
		after:function(){
			$bg_attr = jQuery('.waypoint_active .bg_pane_class').attr("data-attr-bg_pane");
			//console.log('$bg_attr:' + $bg_attr + '!');
			//console.log('scrolled to: ' + $.scrollify.currentIndex() + '!');
			jQuery('#bg_pane').removeClass();
			jQuery('#bg_pane').addClass($bg_attr);
			$id_select = jQuery('.waypoint_active .bg_pane_class').attr("data-attr-id_select");
			jQuery('.grid_item_active').removeClass('grid_item_active');
			if ( '' !== $id_select ) {
				// console.log('$id select:' + $id_select + '!')
				jQuery('#' + $id_select).addClass('grid_item_active');
			}
			/* Debug for fixing iOS display bugs... still not working correctly.
			$('.bg_stack')
			.delay(1500)
			.queue( function(next){ 
				$(this).css('display', 'block');
				console.log('block');
				next(); 
			})
			.delay(10)
			.queue( function(next){ 
				$(this).css('display', 'flex');
				console.log('flex!');
				next(); 
			});
			*/
		},
		afterResize:function() {
			checkSize();
		},
	});
};
function swap_setup(){
	var waypoints = jQuery('.waypoint').waypoint({
		offset: '100%',
		handler: function(direction) {
			jQuery(this.element).addClass('waypoint_loaded').trigger("swap_image");
			this.destroy();
		}
	});
};

jQuery( document ).ready(function() {
	waypoint_directions();
	scrollify_setup();
	objectFitImages();
	checkSize();
	swap_setup();
	waypoint_next();
	waypoint_prev();
	$bg_attr = jQuery('.waypoint_active .bg_pane_class').attr("data-attr-bg_pane");
	jQuery('#bg_pane').removeClass();
	jQuery('#bg_pane').addClass($bg_attr);
	jQuery('#bg_pane').addClass('initial_fade padding_left ');
	$id_select = jQuery('.waypoint_active .bg_pane_class').attr("data-attr-id_select");
	jQuery('.grid_item_active').removeClass('.grid_item_active');
	jQuery($id_select).addClass('.grid_item_active')
	swap_image();
	//console.log('loaded index: ' + $.scrollify.currentIndex() + '!');
	$scrollify_hash = $.scrollify.currentIndex() - 2;
	//console.log('actual hash:' + $scrollify_hash + '!');
	$scroll_target = 'scrollify_pane_' + $scrollify_hash;
	//console.log('Scroll Target:' + $scroll_target + '!');
	jQuery('#' + $scroll_target).addClass('waypoint_active');
});

jQuery(document).on( 'click', '.home_link', function( event ) {
	event.preventDefault();
	var scroll_link = $(this).attr('href');
	$.scrollify.move(scroll_link);
})

jQuery(document).on( 'click', '.box_link', function( event ) {
	event.preventDefault();
	var scroll_link = $(this).attr('href');
	$.scrollify.move(scroll_link);
})

jQuery(document).on( 'click', '.scroll_next', function( event ) {
	event.preventDefault();
	$.scrollify.next();
})
/*
jQuery(document).on( 'click', '#menu_pane a', function( event ) {
	event.preventDefault();
	var menu_id = $(this).attr('data-href');
	var menu_url = $(this).attr('href');
	// console.log(menu_id);

	$.ajax({
		url: dynamic_load_posts.ajaxurl,
		type: 'post',
		data: {
			action: 'dynamic_load_posts',
			'menu_id': menu_id
		},
		success: function( result ) {
			page_transition_load_init( result);
			console.log(result);
			window.history.pushState("next_page", "Title", menu_url);
			jQuery.scrollify.instantMove("#2");
		},
		error: function( result ) {
			alert( 'error' );
		}
	})
})
*/
jQuery( document ).ajaxComplete(function() {
	// console.log('ajaxcomplete!');
	jQuery.scrollify.update();
	Waypoint.refreshAll();
	waypoint_next();
	waypoint_prev();
	jQuery('.waypoint').first().addClass('waypoint_active');
});