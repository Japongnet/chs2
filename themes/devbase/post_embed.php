
<div class="content_row <?php the_sub_field('post_embed_container_class'); ?>" >
	<div class="content_box post_embed">
		<?php the_sub_field('post_embed_before'); ?>
		<div class="post_embed_container">

			<?php

			$args = array(

				'post_type' => get_sub_field('post_embed_type'),
				'posts_per_page' => get_sub_field('posts_per_page'),
				'tag' => get_sub_field('post_embed_tag'),
				'category_name' => get_sub_field('post_categories')

			);

			// The Query
			$query1 = new WP_Query( $args );

			// The Loop
			while ( $query1->have_posts() ) {
				$query1->the_post(); ?>
				<div class=" col-xs-12 col-sm-6 col-md-4 ">
				<a href="<?php the_permalink(); ?>" class="post_embed_single <?php the_sub_field('post_container_class'); ?>">


					<?php if( get_the_post_thumbnail() ){ ?>
						<?php the_post_thumbnail( array(360, 200) ); ?>
					<?php } else { ?>

						<div class="post_embed_image"><?php $image_placeholder = get_field('chronic_placeholder', 17961); ?>

							<img src="<?php echo $image_placeholder[sizes][large]; ?>" alt="Chronic Ink Logo"; />
						</div>
					<?php } ?>
					<div class="post_embed_text">
						<span class="tag"><?php $post_tags = get_the_category();
						if ( $post_tags ) {
							echo $post_tags[0]->name;
						} else { ?>
							Chronic Ink
						<?php } ?></span>

						<h4><?php the_title(); ?></h4>
						<p><?php the_excerpt(); ?></p>

					</div>

				</a>
			</div>
			<?php }

			/* Restore original Post Data
			* NB: Because we are using new WP_Query we aren't stomping on the
			* original $wp_query and it does not need to be reset with
			* wp_reset_query(). We just need to set the post data back up with
			* wp_reset_postdata().
			*/
			wp_reset_postdata();




			?>
		</div>

		<?php the_sub_field('post_embed_after'); ?>

	</div>
</div>
