

  <div class="menu_section sticky_bottom background_indigo">
    <div class="menu_content ">
      <!-- nav -->
      <nav class="nav">
        <div class="header_home_logo_icon bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div>
        <ul>
          <?php 		wp_nav_menu(array ('menu' => 'main_menu', 'items_wrap' => '%3$s'));
          ?>
          <!--add in a separate link inside the menu -->

        </ul>

      </nav>



    </div>
  </div>

</header>

<span class="header_toggle">
</span>
