<div <?php if( get_sub_field('page_section_id') ): ?>
  id="<?php the_sub_field('page_section_id'); ?>"
<?php endif; ?>
class="waypoint section <?php the_sub_field('page_section_class'); ?>"
<?php if( get_sub_field('page_section_bg_placeholder') ): ?>
  data-alternate-src="background-image:url('<?php the_sub_field('page_section_background');?>"
<?php else: ?>
  style="background-image:url('<?php the_sub_field('page_section_background');?>');"
<?php endif; ?>
>

<div class="section_overlay">
</div>
<div class="section_content">

  <div class="section_content_main ">

    <div class="section_content_text">

      <?php the_content(); ?>

      <?php if( get_sub_field('page_section_title') ): ?>
        <div class="section_title"><h2><?php the_sub_field('page_section_title'); ?></h2></div>
      <?php endif; ?>
      <?php if( get_sub_field('page_section_tagline') ): ?>
        <em class="section_tagline"><?php the_sub_field('page_section_tagline'); ?></em>
      <?php endif; ?>
      <div class="clearfix"></div>
      <?php if( get_sub_field('page_section_text') ): ?>
        <div class="section_text">
          <?php the_sub_field('page_section_text'); ?></div>
        <?php endif; ?>
        <?php if( get_sub_field('page_section_button') ): ?>
          <button class="section_button"><?php the_sub_field('page_section_button'); ?></button>
        <?php endif; ?>
        <?php   if (have_rows('excerpt_repeater')) {
          echo '<p> Latest News</p>';
          while ( have_rows('excerpt_repeater') ) : the_row();
          ?>
          <div class="test">

            <?php
            $post_object_test = get_sub_field('post_excerpt');

            $args = array(
              'p' => $post_object_test[0]->ID,
              'post_type' => 'any'
            );

            // The Query
            $the_query = new WP_Query( $args );

            // The Loop
            if ( $the_query->have_posts() ) {

              while ( $the_query->have_posts() ) {
                $the_query->the_post();
                echo '<div class="col-xs-12 col-sm-6 col-lg-4"> ' . get_the_title();
                the_permalink();
                the_field('blog_section_excerpt');
                echo '</div>';
              }

              /* Restore original Post Data */
              wp_reset_postdata();
            } else {
              // no posts found
            }
            rewind_posts();
            ?>
          </div>
          <?php
        endwhile;
      } ?>
    </div>
  </div>
</div>
</div>
