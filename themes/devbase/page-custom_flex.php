<?php
/*
Template Name: Custom Flexible Page
*/
?>

<?php get_header(); ?>

<?php
$i = 1;


// check if the flexible content field has rows of data
if( have_rows('flex_content') ):
	
	// loop through the rows of data
	while ( have_rows('flex_content') ) : the_row(); 
	
	?>

	<?php if ($i == 2): ?>
			<div class="menu_section sticky_bottom background_indigo">
			<div class="menu_content ">
			<!-- nav -->
			<nav class="nav">
				<div class="header_home_logo_icon bg_contain" style="background-image:url('<?php the_field('header_home_logo', 27);?>')"></div>
				<ul>
				<?php 		wp_nav_menu(array ('menu' => 'main_menu', 'items_wrap' => '%3$s'));
				?>
				<!--add in a separate link inside the menu -->

				</ul>

			</nav>



			</div>
		</div>


		<span class="header_toggle">
		</span>
	<?php endif; ?>


	<?php if( get_row_layout() == 'full_width' ):  ?>




	<div class="content_row waypoint <?php the_sub_field('full_width_content_class'); ?>
		<?php if(get_sub_field('black_overlay')): ?>
			<div class="black_overlay"></div>
		<?php endif;
		?>
		<?php the_sub_field('full_width_image_class'); ?>"
		<?php if(get_sub_field('full_width_image_conditional') == "Repeated Image (tiled)"){
			$image = get_sub_field('full_width_image');
			?>
			style="background-image:url('<?php echo $image['url']; ?>');"
			<?php } ?> >
			<?php if(get_sub_field('full_width_image_conditional') != "No Image"){
				$image = get_sub_field('full_width_image');
			} ?>
			<div class="content_wrapper">
				<?php the_sub_field('full_width_content'); ?>
			</div>
			<?php if(get_sub_field('full_width_image_conditional') == "Background Photo"){ ?>
				<?php if(get_sub_field('full_width_image')){
					$image = get_sub_field('full_width_image'); ?>

					<img class="background_image content_wrapper_image <?php the_sub_field('full_width_image_class'); ?>" src="<?php echo $image['sizes']['thumbnail']; ?>"  srcset="<?php echo $image['sizes']['medium'] .' '. $image['sizes']['medium-width'] .'w,'; ?>  <?php echo $image['sizes']['large'] .' '. $image['sizes']['large-width'] .'w, '; ?> <?php echo $image['url'] .' '. $image['width'] .'w '; ?>" sizes="100vw" alt="<?php echo $image['title'] ?>" />
					<?php } ?>
					<?php  } ?>
				</div>

			<?php elseif( get_row_layout() == 'multibox' ): ?>

				<?php include 'multibox.php'; ?>




			<?php elseif( get_row_layout() == 'link_repeater' ): ?>

				<?php include 'link_repeat.php'; ?>

			<?php elseif( get_row_layout() == 'post_embed' ): ?>

				<?php include 'post_embed.php'; ?>

			<?php elseif( get_row_layout() == 'post_slider' ): ?>

				<?php include 'post_slider.php'; ?>


			<?php endif;


		$i++; 			
		endwhile;

		else :

			// no layouts found

		endif;

		?>


		<?php get_footer('new'); ?>
