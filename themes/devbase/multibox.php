<!--Multibox-->
<div class="content_row waypoint <?php the_sub_field('multibox_class'); ?>">
<?php if( have_rows('multibox_repeater') ):
  while ( have_rows('multibox_repeater') ) : the_row(); ?>
  <?php if(get_sub_field('multibox_image')){
    $image = get_sub_field('multibox_image');
  } ?>
 

  <div class="content_box <?php the_sub_field('multibox_content_class'); ?>"   <?php if(get_sub_field('multibox_image_conditional') == "Repeated Image (tiled)"){ ?>style="background-image:url('<?php echo $image['url']; ?>');"<?php } ?>  >  <!-- End of opening div tag -->
    <?php if(get_sub_field('multibox_overlay')): ?>
      <div class="multibox_overlay" style="background-color:<?php the_sub_field('multibox_overlay'); ?>; opacity:<?php the_sub_field('multibox_overlay_opacity'); ?>">

      </div>

    <?php endif;
    ?>
      <?php the_sub_field('multibox_content'); ?>

    <?php if(get_sub_field('multibox_image_conditional') == "Background Photo"){ ?>
      <?php if(get_sub_field('multibox_image')){
        $image = get_sub_field('multibox_image'); ?>
        <?php
      $webp_image =  $image;
      array_walk_recursive(
        $webp_image,
        function (&$value) {
          $formats = array("png", "jpg");
          $webp_formats  = array("png.webp", "jpg.webp");
            $value = str_replace($formats, $webp_formats, $value);
        }
      );
   
        ?>
    
        
        
       

        <picture class="background_image content_wrapper_image placeholder_image <?php the_sub_field('multibox_image_class'); ?>">
       
          <source class="placeholder_image" type="image/webp"
          srcset="<?php the_field('placeholder_image', 27); ?>"
                      data-srcset="
                          <?php echo $webp_image['sizes']['medium'] .' '. $webp_image['sizes']['medium-width'] .'w,'; ?>
                          <?php echo $webp_image['sizes']['large'] .' '. $webp_image['sizes']['large-width'] .'w, '; ?>
                          <?php echo $webp_image['url'] .' '. $webp_image['width'] .'w '; ?>
                          "
                  />
          <source class="placeholder_image"
          srcset="<?php the_field('placeholder_image', 27); ?>"
          data-srcset="<?php echo $image['sizes']['medium'] .' '. $image['sizes']['medium-width'] .'w,'; ?>
                          <?php echo $image['sizes']['large'] .' '. $image['sizes']['large-width'] .'w, '; ?>
                          <?php echo $image['url'] .' '. $image['width'] .'w '; ?>
                          "
                  />
          <img src="<?php echo $image['sizes']['thumbnail']; ?>"
          alt="<?php echo $image['title'] ?>" />
        </picture>

        <?php } ?>
        <?php  } ?>
    <?php if(get_sub_field('multibox_image_conditional') == "Picture Element"){ ?>
      <?php if(get_sub_field('multibox_image')){
        $image = get_sub_field('multibox_image');
        $image_mobile = get_sub_field('multibox_image_mobile');
        $formats = array("png", "jpg");
        $webp_formats  = array("png.webp", "jpg.webp");
        $webp_image =  $image;
       
        array_walk_recursive(
          $webp_image,
          function (&$value) {
            $formats = array("png", "jpg");
            $webp_formats  = array("png.webp", "jpg.webp");
              $value = str_replace($formats, $webp_formats, $value);
          }
        );
        $webp_image_mobile =  $image_mobile;
        
        if(get_sub_field('multibox_image_mobile')){
       
        array_walk_recursive(
          $webp_image_mobile,
          function (&$value) {
            $formats = array("png", "jpg");
            $webp_formats  = array("png.webp", "jpg.webp");
              $value = str_replace($formats, $webp_formats, $value);
          }
        );

        };
        
        ?>
      
    

        <picture class="background_image content_wrapper_image <?php the_sub_field('multibox_image_class'); ?>">
          <source class="placeholder_image" srcset="<?php the_field('placeholder_image', 27); ?>" data-srcset="<?php echo $webp_image['sizes']['medium'] .' '. $webp_image['sizes']['medium-width'] .'w,'; ?>  <?php echo $webp_image['sizes']['large'] .' '. $webp_image['sizes']['large-width'] .'w, '; ?> <?php echo $webp_image['url'] .' '. $webp_image['width'] .'w '; ?>" media="(min-width: 769px)" type="image/webp">
          <source class="placeholder_image" srcset="<?php the_field('placeholder_image', 27); ?>" data-srcset="<?php echo $image['sizes']['medium'] .' '. $image['sizes']['medium-width'] .'w,'; ?>  <?php echo $image['sizes']['large'] .' '. $image['sizes']['large-width'] .'w, '; ?> <?php echo $image['url'] .' '. $image['width'] .'w '; ?>" media="(min-width: 769px)">
          <source  class="placeholder_image" srcset="<?php the_field('placeholder_image', 27); ?>" data-srcset="<?php echo $webp_image_mobile['url'] .' '. $webp_image_mobile['width'] .'w '; ?>" type="image/webp">
          <source  class="placeholder_image" srcset="<?php the_field('placeholder_image', 27); ?>" data-srcset="<?php echo $image_mobile['url'] .' '. $image_mobile['width'] .'w '; ?>">
          <img src="<?php echo $image['sizes']['thumbnail']; ?>"  srcset="<?php echo $image['sizes']['medium'] .' '. $image['sizes']['medium-width'] .'w,'; ?>  <?php echo $image['sizes']['large'] .' '. $image['sizes']['large-width'] .'w, '; ?> <?php echo $image['url'] .' '. $image['width'] .'w '; ?>" sizes="100vw" alt="<?php echo $image['title'] ?>" />
        </picture>
        <noscript>
        <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['title'] ?>">
        </noscript>

        <?php } ?>
        <?php  } ?>
      </div>

    <?php endwhile;

    else :



     endif; ?>
</div>
