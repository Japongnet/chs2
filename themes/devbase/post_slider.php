<div class="content_row blog_slider_container slider_container white_text">
  <div class="slider">


      <?php

      $args = array(

        'post_type' => get_sub_field('post_embed_type'),
        'posts_per_page' => get_sub_field('posts_per_page'),
        'tag' => get_sub_field('post_embed_tag'),
        'category_name' => get_sub_field('post_categories')


      );

      // The Query
      $query1 = new WP_Query( $args );

      // The Loop
      while ( $query1->have_posts() ) {
        $query1->the_post(); ?>
        <?php $image_slider = get_the_post_thumbnail_url(); ?>

        <div class="slide" style="background-image:url('<?php echo $image_slider; ?>');">
          <div class="gradient_background"></div>







          <div class="post_embed_text blog_slide_content">

            <span class="tag"><h6><?php $post_tags = get_the_category();
						if ( $post_tags ) {
							echo $post_tags[0]->name;
						} else { ?>
							Chronic Ink
						<?php } ?></h6></span>
            <a href="<?php the_permalink(); ?>" class="post_embed_single <?php the_sub_field('post_container_class'); ?>"><h1><?php the_title(); ?></h1></a>
                        <a href="<?php the_permalink(); ?>" class="content_link content_button link_white">READ MORE</a>

          </div>

        </a>
          </div>
      <?php }

      /* Restore original Post Data
      * NB: Because we are using new WP_Query we aren't stomping on the
      * original $wp_query and it does not need to be reset with
      * wp_reset_query(). We just need to set the post data back up with
      * wp_reset_postdata().
      */
      wp_reset_postdata();




      ?>



  </div>
</div>
