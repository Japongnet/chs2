<!-- footer -->
<?php if( have_rows('content_row', 1074) ): ?>
  <?php while(have_rows('content_row', 1074) ): the_row(); ?>
    <div class="content_row" <?php if(get_sub_field('content_row_image')): ?> style="background-image:url(' <?php the_sub_field('content_row_image', 1074); ?>');" <?php endif; ?>>
      <?php if( have_rows('content_item', 1074) ): ?>
        <?php while(have_rows('content_item', 1074)): the_row(); ?>

          <div class="content_item <?php the_sub_field('content_item_class', 1074); ?>"   <?php if(get_sub_field('content_row_image')): ?>
            style="background-image:url('<?php the_sub_field('content_item_image', 1074); ?>');"
          <?php endif; ?>><!-- Add background-image style to content-item if field populated -->
            <?php if(get_row_layout() == 'standard_content'): ?>
              <?php if(get_sub_field('content_item_title', 1074)) : ?>
                <h3><?php the_sub_field('content_item_title', 1074); ?></h3>
              <?php endif; ?>
              <?php if(get_sub_field('content_item_title', 1074)): ?>
                <p><?php the_sub_field('content_item_text', 1074); ?></p>
              <?php endif; ?>
            <?php endif; ?>
          </div> <!-- item  -->

        <?php endwhile; // end of content_item. ?>
      <?php endif ; // content_item  ?>
    </div> <!-- Row  -->
  <?php endwhile; // end of content_row. ?>
<?php endif ; // content_row ?>


<span class="footer_toggle">
</span>
