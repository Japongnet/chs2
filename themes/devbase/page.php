<?php
/*
* Template Name: Page Flex box
*/
get_header(); ?>



<?php if( have_rows('content_wrapper') ): ?>

  <?php while(have_rows('content_wrapper') ): the_row(); ?>
    <?php if(get_sub_field('content_wrapper_image_conditional') != "No Image"){
        $image = get_sub_field('content_wrapper_image');
      } ?>
    <div class="content_wrapper <?php the_sub_field('content_wrapper_class'); ?>" <?php if(get_sub_field('content_wrapper_image_conditional') == "Repeated Image (tiled)"){ ?>
    style="background-image:url('<?php echo $image['url']; ?>');" <?php } ?> >


      <?php if(get_sub_field('content_wrapper_image_conditional') == "Background Photo"){ ?>
      <?php if(get_sub_field('content_wrapper_image')){
        $image = get_sub_field('content_wrapper_image'); ?>

      <img class="content_wrapper_image <?php the_sub_field('content_wrapper_image_class'); ?>" src="<?php echo $image['sizes']['thumbnail']; ?>"  srcset="<?php echo $image['sizes']['medium'] .' '. $image['sizes']['medium-width'] .'w,'; ?>  <?php echo $image['sizes']['large'] .' '. $image['sizes']['large-width'] .'w, '; ?> <?php echo $image['url'] .' '. $image['width'] .'w '; ?>" sizes="100vw" alt="<?php echo $image['title'] ?>" />
      <?php } ?>

      <?php  } ?>
      <?php if( get_sub_field('include_default_header') ) : ?>
        <header class="header clear">

          <?php include 'header_custom.php'; ?>
          <?php include 'header_menu.php'; ?>
        </header>
      <?php else: ?>
        <?php if( have_rows('custom_header') ): ?>
            <header>
          <?php while(have_rows('custom_header') ): the_row(); ?>

            <div class="content_row <?php the_sub_field('custom_header_class'); ?>"   style="background-image:url('<?php the_sub_field('custom_header_image'); ?>');">
              <?php if( have_rows('custom_header_item') ): ?>
                <?php while(have_rows('custom_header_item')): the_row(); ?>

                  <?php if(get_row_layout() == 'standard_content'): ?>
                    <div class="content_item <?php the_sub_field('custom_header_item_class'); ?>">
                      <?php if(get_sub_field('custom_header_item_title')): ?>
                        <h3><?php the_sub_field('custom_header_item_title'); ?></h3>
                      <?php endif;?>
                      <?php if(get_sub_field('custom_header_item_text')) : ?>
                        <p><?php the_sub_field('custom_header_item_text'); ?></p>
                      <?php endif;?>
                    </div> <!-- item  -->
                  <?php  endif; ?>

                </div>



              <?php endwhile; // end of custom_header_item. ?>
            <?php endif; // ccustom_header_item  ?>
          <?php endwhile; // end of custom_header. ?>
          <?php include 'header_menu.php'; ?>
        </header>
        <?php endif; //  /custom_header?>

      <?php endif; ?>

      <?php if( have_rows('content_row') ): ?>
        <?php while(have_rows('content_row') ): the_row(); ?>
          <div class="content_row <?php the_sub_field('content_row_class'); ?>" style="background-image:url('<?php the_sub_field('content_row_image'); ?>');">

            <?php if( have_rows('content_item') ): ?>
              <?php while(have_rows('content_item')): the_row(); ?>
                <div class="content_item <?php the_sub_field('content_item_class'); ?>"  style="background-image:url('<?php the_sub_field('content_item_image'); ?>');">

                  <?php if(get_row_layout() == 'standard_content'): ?>


                    <?php if(get_sub_field('content_item_title')): ?>
                      <h3><?php the_sub_field('content_item_title'); ?></h3>
                    <?php endif;?>
                    <?php if(get_sub_field('content_item_text')) : ?>
                      <p><?php the_sub_field('content_item_text'); ?></p>
                    <?php endif;?>
                  <?php  endif;?>
                </div> <!-- item  -->
              <?php endwhile; // end of content_item. ?>
            <?php endif; // content_item  ?>
          </div> <!-- Row  -->
        <?php endwhile; // end of content_row. ?>
      <?php endif; // content_row ?>

      <?php if( get_sub_field('include_default_footer') ) : ?>

        <?php include 'footer_content.php'; ?>

      <?php else: ?>
        <?php if( have_rows('custom_footer') ): ?>

          <?php while(have_rows('custom_footer') ): the_row(); ?>
            <div class="content_row <?php the_sub_field('custom_footer_class'); ?>"   style="background-image:url('<?php the_sub_field('custom_footer_image'); ?>');">
              <?php if( have_rows('custom_footer_item') ): ?>
                <?php while(have_rows('custom_footer_item')): the_row(); ?>

                  <?php if(get_row_layout() == 'standard_content'): ?>
                    <div class="content_item <?php the_sub_field('custom_footer_item_class'); ?>">
                      <?php if(get_sub_field('custom_footer_item_title')): ?>
                        <h3><?php the_sub_field('custom_footer_item_title'); ?></h3>
                      <?php endif;?>
                      <?php if(get_sub_field('custom_footer_item_text')) : ?>
                        <p><?php the_sub_field('custom_footer_item_text'); ?></p>
                      <?php endif;?>
                    </div> <!-- item  -->
                  <?php  endif; ?>

                </div>



              <?php endwhile; // end of custom_footer_item. ?>
            <?php endif; // ccustom_footer_item  ?>
          </div> <!-- wrapper  -->


        <?php endwhile; // end of custom_footer. ?>
      <?php endif; //  /custom_footer?>

    <?php endif; ?>

  <?php endwhile; // end of content_wrapper. ?>
<?php endif; //  /content_wrapper?>



<?php get_footer(); ?>
