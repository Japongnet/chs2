<?php ob_start(); ?>
<div class="product">
    <?php if(  get_field('product_image') ): ?>
      <div class="product_image"><img class="placeholder_image" src="<?php the_field('placeholder_image', 27) ?>" data-alternate-src="<?php the_field('product_image') ?>" alt="<?php the_title() ?>"></div>
    <?php endif; ?>
   <div class="product_details">
     <div class="product_title"><?php the_title() ?></div>
      <div class="product_description"><?php the_field('product_description') ?></div>
      <div class="product_price>"><?php the_field('product_price') ?></div>
      <form action="" method="post">
      <div class="cart_add"><input type="checkbox"  name="cart_add"
       value ="<?php the_ID(); ?>, <?php the_title(); ?>"></div>
       <input type="submit" class="cart_input" name="Submit" value="Submit!" data-cart="<?php the_ID(); ?>"/>
     </form>
  </div>
</div>
<?php $cart_id = ob_get_contents();
ob_end_clean();
?>
